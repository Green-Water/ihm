import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Fenetre extends JFrame implements ActionListener {
	
	//Cr�ation des diff�rents conteneurs Panel
	JPanel content = new JPanel();  				//Conteneur principal content
	JPanel content2 = new JPanel();
	JPanel content3 = new JPanel();
	
	//Classe qui lit dans la BDD
	Read_BDD Lecture = new Read_BDD(); 	 			//Cr�ation d'une instance BDD
	
	//Cr�ation d'un Label
	JLabel label = new JLabel();					
	JLabel label2 = new JLabel();
	JLabel label3 = new JLabel();
	
	public Fenetre(){											//Constructeur
		
		 //Creation de la fenetre
		 setTitle("IHM premi�re version Java");               	//Titre
		 setSize(500, 500);										//Taille
		 setLocationRelativeTo(null); 							//Centrage
		 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 		//Arret du programme
		 
		 //Les panels du conteneurs principal
		 content.setLayout(new GridLayout(2,2));		//content est un tableau (2,2)
		 JPanel p1 = new JPanel();						//Panel p1 
		 Panneau p2 = new Panneau();					//Panneau p2
		 JPanel p3 = new JPanel();						//Panel p3
		 JPanel p4 = new Panneau();							
		 p4.setLayout(new BorderLayout());
		 
		 //Les conteneurs secondaires
		 content2.setBackground(Color.BLUE);
		 content3.setBackground(Color.green);
		 
		 //Menu
		 JMenuBar menubarre = new JMenuBar();
		 
		 JMenu Home = new JMenu ("Home");					//Menu 'Home' 
		 JMenuItem Home2 = new JMenuItem("Home");
		 Home2.addActionListener(this);						//Definition d'un listener
		 
		 JMenu Stat = new JMenu ("Stats"); 					//Menu 'Stats'
		 JMenuItem Hebdo = new JMenuItem("Hebdo");
		 Hebdo.addActionListener(this);						//Listener
		 JMenuItem Mensuel = new JMenuItem("Mensuel");
		 Mensuel.addActionListener(this);					//Listenet
		 
		 Stat.add(Hebdo);
		 Stat.add(Mensuel);
		 Home.add(Home2);
		 menubarre.add(Home);
		 menubarre.add(Stat);
		 setJMenuBar(menubarre);
		 
		 //Test lecture BDD
		 System.out.print(Lecture.x);
		 
		 //Les boutons
		 JButton rafraichir = new JButton("Rafraichir l'affichage");
		 rafraichir.addActionListener(this);
		
		 //Les labels
		 label.setText(Lecture.z);						
		 label2.setText(Lecture.y);
		 label3.setText(Lecture.x);
		 
		 //Le contenu des Panels
		 p1.setBackground(Color.BLUE);					//Couleur 
		 p3.setBackground(Color.GREEN);
		 p1.add(label);									//Insertion des labels
		 p3.add(label2);
		 p2.add(label3);
		 p4.add(rafraichir,BorderLayout.CENTER); 		//Insertion du bouton dans p4
		 
		 //La mise en page
		 content.add(p1);								//Ajout des panneaux p dans content
		 content.add(p2);								
		 content.add(p3); 								
		 content.add(p4);
		 
		 setContentPane(content);					//Ajout de content en content pane
		
		 //L'affichage
		 setVisible(true);							// Affichage de la fenetre
		  }
	//R�ponse aux Listeners
	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if(cmd.equals("Hebdo")) {					//Changement de conteneur
			this.setContentPane(content2);
			this.validate();
		}
		else if(cmd.equals("Home")) { 				//Changement de conteneur
			this.setContentPane(content);
			this.validate();
		}
		else if(cmd.equals("Mensuel")) {		    //Changement de conteneur
			this.setContentPane(content3);
			this.validate();
		}
		else if(cmd.equals("Rafraichir l'affichage")) {		//Rafraichir l'affichage
			Read_BDD Lecture2 = new Read_BDD();
			label.setText(Lecture2.z);						
			label2.setText(Lecture2.y);
			label3.setText(Lecture2.x);
			this.validate();
		}
		
	}
}
