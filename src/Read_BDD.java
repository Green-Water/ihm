import java.io.BufferedReader;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
//import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Read_BDD { 
		//Attributs
		public String x = null;
		public String y = null;
		public String z = null;
		
		//Constructeur
		public Read_BDD(){ 
			String LigneLu = null;
			int compteur = 0;
			try {
				FileInputStream BDD = new FileInputStream("BDD.txt");
				InputStreamReader FR = new InputStreamReader(BDD);
				BufferedReader BF = new BufferedReader(FR);
				while ((LigneLu = BF.readLine()) != null) {        //On parcourt chaque ligne
					
					//System.out.println(LigneLu);
					if (compteur == 0) {
						x = LigneLu;
					}
					if (compteur == 1) {
						y = LigneLu;
					}
					if (compteur == 2) {
						z = LigneLu;
					}
					compteur ++; 
				}
				//System.out.print(x);
				//System.out.print(y);
				//System.out.print(z);
				BF.close();
				
			}catch (FileNotFoundException e) {
				System.err.printf("fichier %s non trouv�");//, BDD.toString());
			} catch (IOException e) {
				System.err.println("Impossible de lire le contenu de %s");// + BDD.toString());
			}
			
		}
}
